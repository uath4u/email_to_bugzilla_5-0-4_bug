#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Dieses Scritp überprüft die Inbox einem Imap Account und erstellt aus allen dort vorliegenden
# Mails einen Bug in Bugzilla (5.0.4). Sind Anhänge oder Bilder in der Mail, werden diese als Anhang an den Bug
# hochgeladen. Jede bearbeitete Mail wird danach in den Ordner INBOX/erstellte_bugs verschoben.

# Author: Lars Schwarzer
# Date: 25.08.2020

import email
import email.parser
import imaplib
import requests
import json
import logging
import warnings
import re
import smtplib
import pandas as pd

from bs4 import BeautifulSoup
from urllib3.exceptions import InsecureRequestWarning
from email.header import decode_header
from base64 import b64encode
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from pathlib import Path


class EmailToBug:
    """Main Class to put information from an email to bugzilla

    Modules

    get_mails:                       get the mails from imap-server
    _post_attachments_to_bug:        Create new attachment in bugzilla from bug attachment
    _create_new_bug:                 Creates a new bug from email
    _get_token:                      gets a new usertoken from bugzilla if old token is invalid
    _post_email_to_bug:              put subject and text from email to bug as a comment
    _check_subject_for_bug_number:   checks if there is a bug number in format [Bug xxx] in email subject and returns it
    _aggregate_bug_numbers:          aggregate bug numbers for each email if there is [Bug xxx] in email subject
    _aggregate_milestones:           Send email_from and email_subject from self.all_email_dataframe to
                                     search_milestone_in_subject().
    _search_milestone_in_subject:    aggregate milestones for each email_subject send by aggregate_milestones() if there
                                     is on of the keys from milestone_dict is found or.
    _cleanup_all_supjects:           Send all subjects from self.all_email_dataframe to cleanup_subject().
    _cleanup_subject:                Removes all patterns from self.possible_bug_numbers, self.milestone_dict and
                                     self.wg_patterns_list from subject and returns it to self.all_emai_dataframe
    main:                            Run class as script
    _send_mail:                      Sends response email
    _build_response_email:           Builds response email for created bug

    Attributes

    self.all_email_dataframe:       main DataFrame() containing all data
    self.login_dict:                Dictionary with bugzillatoken and bugzilla-ID
    self.possible_bug_numbers:      List with res to match at check_subject_for_bug_number()
    self.milestone_dict:            Dictionary with all milestones matched in earch_milestone_in_subject()
    self.wg_patterns_list:          List with patterns belongingto forwarded mails, to remove in cleanup_subject()
    self.cc_ignore_list:            List containing all accounts to be ignored by setting email_from to cc-list of new
                                    tickets
    self.allowed_domains_list       List containing all domains to create a buck for
    self.allowed_emails_address_list
                                    List containing all allowed email addresses to create a buck for
    self.no_response_email_list     List containing all email addresses don't send a response email when a new buck has
                                    been created
    """

    def __init__(self):
        """Instantiating Class EmailToBug

        self.all_email_dataframe is setting up as an instance of pd.DataFrame()
        sel.possible_bug_numbers is setting up as an instance of list
        self.milestone_dict is setting up as an instance of dictionary
        self.wg_patterns_list is setting up as an instance of list
        self.email_to_milestone_dict is setting up as an instance of dictionary
        self.login_dict is setting up as an dictionary containing bugzillatoken and bugzilla-ID
        self.cc_ignore_list is setting up as an instance of list
        self.allowed_domains_list is setting up as an instance of list
        self.allowed_emails_address_list is setting up as an instance of list
        self.no_response_email_list is setting up as an instance oof list
        """

        self.all_email_dataframe = pd.DataFrame(columns=['email_from', 'email_subject', 'email_text', 'bug_priority',
                                                         'attachments'])
        self.possible_bug_numbers = [r'#([0-9]+):', r'\[Ticket ([0-9]+)]', r'\[Bug ([0-9]+)]']
        self.milestone_dict = {'(INSERT MILESTONE SHORTCUT)': 'INSERT MILESTONE NAME', }
        script_path = Path(__file__).resolve().parent
        self.cc_ignore_list = self._get_list_from_cfg(
            cfg_path=f'{script_path}/cc_ignore.cfg')
        self.wg_patterns_list = ['WG:', 'FW:']
        self.allowed_domains_list = self._get_list_from_cfg(
            cfg_path=f'{script_path}/allowed_domains.cfg')
        self.allowed_emails_address_list = self._get_list_from_cfg(
            cfg_path=f'{script_path}allowed_email_addresses.cfg')
        self.no_response_email_list = self._get_list_from_cfg(
            cfg_path=f'{script_path}/no_response_email.cfg')
        self.__login_dict = {'token': self._get_token()}

    def get_emails(self):
        """get the mails from imap-server

        get_emails(self) -> None

        Function

        Gets all emails from it-ticket@/INBOX via imap and put their data to self.all_email_dataframe.

        Stored data in self.all_email_dataframe:

        email_from
        email_subject
        email_text
        email_priority Low, Normal or High
        attachments
        milestone

        Module Functions:

        _get_email_from:                 Parse email address of sender from email
        _add_sender_name_to_subject:     Adds the name of mail sender to email subject
        _get_subject:                    Parse email subject from email
        _get_bug_priority:               Parse email priority from email and transcript it to bug priority
        _get_email_text_and_attachments: Parse email text and attachments from email
        _get_email_data:                 Gets email data from smtp server
        _get_mail_ids:                   Gets email ids from all email in Inbox via smtp
        _move_mail_to_folder:            Moves email to specified folder
        _check_email_for_autoresponder:  Checks if a email is an autoresponder email
        _email_main:                     Controls workflow of get_emails
        """

        logging.info('Emails werden abgeholt')

        __user = 'IMAP USER NAME'
        __pw = 'IMAP PASSWORD'
        # Insert imap server name here
        server = "IMAP SERVER NAME"
        port = 993
        imap_source_folder = 'INBOX'

        def _get_email_from(mail_data):
            """
            Parse email address of sender from email

            _get_email_from(mail_data -> bytes) -> str

            :param mail_data: (bytes) contains mail data
            :return: (str) email address of sender
            """
            mail_address_unclean = str(mail_data['From'])
            return mail_address_unclean[mail_address_unclean.find('<') + 1:].rstrip('>')

        def _add_sender_name_to_subject(mail_address, mail_subject):
            """
            Adds the name of mail sender to email subject

            _add_sender_name_to_subject(mail_address -> str, mail_subject -> str) -> str

            :param mail_address: (str) Mail address of sender
            :param mail_subject: (str) Mail subject of mail
            :return: (str) Subject with sender name added
            """

            from_part_list = mail_address[0:mail_address.find('@')].split('.')
            sender_name_clean = ''

            for from_part in from_part_list:

                from_part = from_part[0].upper() + from_part[1:]

                if from_part != 'Ext':
                    if not sender_name_clean:
                        sender_name_clean += from_part

                    else:
                        sender_name_clean += f' {from_part}'

            return mail_subject + f' [{sender_name_clean}]'

        def _get_subject(mail_data, mail_address):
            """
            Parse email subject from email and send it to _add_sender_name_to_subject()

            _get_subject(mail_data -> bytes, mail_address -> str) -> str

            :param mail_data: (bytes) containing mail data
            :param mail_address: (str) containing mail address
            :return: (str) containing subject with name of sender added
            """

            subject_from_mail_header = decode_header(mail_data['Subject'])
            subject_text = ''

            for subject_part in subject_from_mail_header:

                text_part = subject_part[0]

                if isinstance(text_part, bytes):
                    if subject_part[1] is None:
                        text_part = text_part.decode()

                    else:
                        text_part = text_part.decode(subject_part[1])

                subject_text += text_part

            subject_text = _add_sender_name_to_subject(mail_address, subject_text)

            return subject_text

        def _get_bug_priority(mail_data):
            """
            Parse email priority from email and transcript it to bug priority

            gat_but_priority(mail_data -> bytes) -> str

            :param mail_data: (bytes) containing mail data
            :return: (str) Containing bug priority set by email priority
            """

            if not (priority_from_mail := mail_data['X-Priority']):
                return 'Normal'

            elif priority_from_mail == '5':
                return 'Low'

            elif priority_from_mail == '1':
                return 'High'

        def _get_email_text_and_attachments(mail_data):
            """
            Parse email text and attachments from email

            _get_email_text_and_attachments(mail_data -> bytes) -> str, list

            :param mail_data: (bytes) containing mail data
            :return: (str) containing email text, (list) containing email attachments
            """

            attachment_list = []

            def _add_attachment(content_type_to_attach, data_to_attach, content_filename):
                """
                adds one attachment_list to the attachmentlist of an specific email.

                _add_attachment(content_type_to_attach -> str, data_to_attack -> bytes, content_filename -> str) -> None

                :param content_type_to_attach:  (str) type of content like application/pdf to attack to attachments
                :param data_to_attach:          (bytes) bytes of data to be attached
                :param content_filename:        (str) filename from email
                :return:                        None
                """

                attachment_list.append({'content_type_to_attach': content_type_to_attach,
                                        'data_to_attach': data_to_attach,
                                        'content_filename': content_filename})

            email_text_html = ''
            email_text_plain = ''
            attachment_list.clear()

            for content in mail_data.walk():

                # Getting email text

                if (type_of_content := content.get_content_type()) == 'text/html':

                    if email_text_html == '':
                        email_text_html = BeautifulSoup(content.get_payload(decode=True), 'html.parser') \
                            .get_text()

                    else:
                        email_text_html = '{}\n\n{}'.format(email_text_html,
                                                            BeautifulSoup(content.get_payload(decode=True),
                                                                          'html.parser').get_text())

                elif type_of_content == 'text/plain':

                    if email_text_plain == '':
                        email_text_plain = BeautifulSoup(content.get_payload(decode=True), 'html.parser') \
                            .get_text()

                    else:
                        email_text_plain = '{}\n\n{}'.format(email_text_plain,
                                                             BeautifulSoup(content.get_payload(decode=True),
                                                                           'html.parser').get_text())

                # Getting attachments

                elif (actual_content_filename := content.get_filename()) and type_of_content not in \
                        ['message/rfc822', 'message/delivery-status']:

                    _add_attachment(type_of_content, content.get_payload().encode('ascii'),
                                    actual_content_filename)

                elif type_of_content in ['message/rfc822', 'message/delivery-status']:

                    if type_of_content == 'message/rfc822':
                        type_of_content = 'application/x-ole-storage'
                        subject_from_mail_header = decode_header(content.get_payload()[0]['subject'])
                        mail_file_name = subject_from_mail_header[0][0]
                        mail_header_encoding = subject_from_mail_header[0][1]

                        if type(mail_file_name) == bytes:
                            mail_file_name = mail_file_name.decode(mail_header_encoding)

                    elif type_of_content == 'message/delivery-status':
                        mail_file_name = content.get_filename()

                    else:
                        mail_file_name = 'no filename found'

                    base64_bytes = b64encode(content.get_payload()[0].as_string().encode("ascii"))
                    _add_attachment(type_of_content, base64_bytes, mail_file_name)

            if email_text_html:
                email_text = email_text_html

            elif email_text_plain:
                email_text = email_text_plain

            else:
                email_text = 'No Text found'

            if not attachment_list:
                _add_attachment('', '', '')

            return [email_text, attachment_list]

        with imaplib.IMAP4_SSL(server, port) as imap_connection:

            def _get_email_data(actual_id):
                """
                Gets email data from smtp server

                _get_email_data(actual_id -> bytes) -> email.email

                :param actual_id: (bytes) containing id of actual email to collect data for
                :return: (email.email) containing actual email
                """

                logging.info('Emails werden abgeholt und ausgewertet')

                logging.info(f'Email mit der ID {actual_id} wird jetzt abgeholt')
                mail_raw_data = imap_connection.fetch(actual_id, "(RFC822)")[1][0][1]

                return email.parser.BytesParser().parsebytes(mail_raw_data)

            def _get_mail_ids():
                """
                Gets email ids from all email in Inbox via smtp

                _get_mail_ids() -> list

                :return: list containing bytes of all email ids
                """

                imap_connection.login(__user, __pw)
                imap_connection.select(imap_source_folder)
                status, mail_data = imap_connection.search(None, 'ALL')

                if len(mail_ids := mail_data[0].split()) == 0:
                    logging.info('Keine Emails vorhanden')
                    exit(0)

                else:
                    while mail_ids:
                        yield mail_ids.pop()

            def _move_mail_to_folder(move_id, move_destination):
                """
                Moves email to specified folder

                maove_mail_to_folder(move_id -> bytes, move_destination -> str) -> None

                :param move_id: (bytes) containing email id number
                :param move_destination: (str) containing destination folder
                :return: None
                """

                imap_connection.copy(move_id, move_destination)
                imap_connection.store(move_id, '+FLAGS', '\\Deleted')

            def _check_email_for_autoresponder(email_to_check, actual_id):
                """
                Checks if a email is an autoresponder email

                _check_email_for_autoresponder(email_to_check -> bytes, actual_id -> bytes) -> True or False

                :param email_to_check: (bytes) containing email data
                :param actual_id: (bytes) containing id of email
                :return: True or False
                """

                if email_to_check['X-Auto-Response-Suppress'] == 'All':
                    logging.info('Found an autorespondermail')
                    _move_mail_to_folder(actual_id, 'INBOX/Autoresponder')
                    return True
                return False

            def _email_main():
                """
                Controls workflow of get_emails

                _email_main() -> None

                :return: None
                """

                for actual_id in _get_mail_ids():

                    mail_data = _get_email_data(actual_id)

                    if _check_email_for_autoresponder(mail_data, actual_id):
                        continue

                    _move_mail_to_folder(actual_id, 'INBOX/erstellte_bugs')
                    email_from = _get_email_from(mail_data)
                    email_body_parsed = _get_email_text_and_attachments(mail_data)
                    mail_data_dict = {'email_from': email_from,
                                      'email_subject': _get_subject(mail_data, email_from),
                                      'email_text': email_body_parsed[0],
                                      'bug_priority': _get_bug_priority(mail_data),
                                      'attachments': email_body_parsed[1]}
                    self.all_email_dataframe = self.all_email_dataframe.append(mail_data_dict, ignore_index=True)

                imap_connection.expunge()
                self._check_dataframe_for_editable_datasets()

            _email_main()

    def _send_mail(self, response_email):
        """
        Send email via smtp

        _send_mail(self, response_email -> email.mime.multipart.MIMEMultipart) -> None

        :param response_email: email.mime.multipart.MIMEMultipart containing response email
        :return: None
        """
        # Insert smtp server name below
        with smtplib.SMTP('SMTP SERVER NAME', 587) as smtp_client:
            smtp_client.starttls()
            smtp_client.ehlo()
            # Set smtp username and password below
            smtp_client.login(user=f'SMTP USER NAME', password='SMTP PASSWORD')
            smtp_client.send_message(response_email)

    @staticmethod
    def _build_response_mail(ticket_number, email_to):
        """
        Build response email for new bugs

        _build_mail(ticket_number -> str, email_to -> str) -> email.mime.multipart.MIMEMultipart

        :param ticket_number:   str containing the ticket number for the email
        :param email_to:        str containing the email address to send the email to
        :return:                email.mime.multipart.MIMEMultipart containing the response email
        """

        response_email = MIMEMultipart()
        # Insert domain name here
        response_email['From'] = 'bugzilla@DOMAIN NAME'
        response_email['To'] = email_to
        response_email['Subject'] = f'Dein [Ticket {ticket_number}] wurde erstellt. Hilfe naht! :)'
        # Insert email response text here
        response_email.attach(MIMEText('RESPONSE TEXT'))
        return response_email

    def _check_dataframe_for_editable_datasets(self):
        """
        Checks after collecting emaildata, if there are emails to build a ticket for. If there are no emails, the script
        will exit with exitcode 0.

        check_dataframe_for_editable_datasets() -> None or exit(0)

        :return: None or exit(0)
        """

        if len(self.all_email_dataframe.index) == 0:

            logging.info('No emails to build a ticket from')
            exit(0)

    def post_attachments_to_bug(self):
        """post attachment to a bug identified by bugnumber as attachment.

        post_attachments_to_bug(self) -> None

        Function:

        post attachment to a bug identified by bugnumber as attachment using bugzilla 5.0.4 web api.
        """

        logging.info('Attachments werden angehängt')

        for row in self.all_email_dataframe.itertuples():

            for attachment in row.attachments:

                if attachment['data_to_attach'] != '':
                    call_data = {'Bugzilla_token': self.__login_dict['token'],
                                 'data': attachment['data_to_attach'],
                                 'file_name': attachment['content_filename'],
                                 'summary': attachment['content_filename'],
                                 'content_type': attachment['content_type_to_attach']}
                    #Insert bugzilla server address below
                    requests.post('BUGZILLA SERVER ADRESS/rest.cgi/bug/{0}/attachment'
                                  .format(row.bug_number), verify=False, data=call_data)

    def create_new_bug(self):
        """Create a new bug in bugzilla

        create_new_bug(self) -> None

        Function

        Create a new bug in bugzilla using the bugzilla 5.0.4 web api for
        every Email without [Bug xxx] (xxx is Bugnumber) in subject.
        Returned bug-ID from bugzilla is appended to emails dataset.
        Bugreporter after all will be set to cc-list of the new bug by calling self.set_email_from_to_cc_list
        """

        logging.info('Bugs werden erzeugt')

        for row in self.all_email_dataframe[self.all_email_dataframe['bug_number'] == 0].itertuples():

            call_data_dict = {"product": "DMM",
                              "component": "Allgemein",
                              "summary": row.email_subject,
                              "version": "unspecified",
                              "priority": row.bug_priority,
                              "rep_platform": "All",
                              "description": "From:\n\n{0}\n\n{1}".format(row.email_from,
                                                                          row.email_text.lstrip('\n')),
                              "target_milestone": row.milestone
                              }
            call_data = json.dumps(call_data_dict)
            call_headers = {'Content-Type': 'application/json'}
            #Insert bugzilla server address below
            response = requests.post('BUGZILLA SERVER ADRESS/rest.cgi/bug?Bugzilla_token={}'
                                     .format(self.__login_dict['token']), headers=call_headers, verify=False,
                                     data=call_data)
            self.all_email_dataframe.at[row.Index, 'bug_number'] = (ticket_number := json.loads(response.text)['id'])
            self._set_email_from_to_cc_list(row.email_from, ticket_number)

            if row.email_from not in self.no_response_email_list:
                self._send_mail(response_email=self._build_response_mail(ticket_number=ticket_number,
                                                                         email_to=row.email_from))

    def _set_email_from_to_cc_list(self, bug_reporter, bug_number):
        """Sets email_from to cc-List of the new bug.

        set_email_from_to_cc_list(self, bug_reporter -> String, bug_number -> String) -> None

        Function

        Sets sender of the email to cc list of the new bug by calling bugzilla 5.04 rest api if sender not in
        cc_ignore_list.
        """

        if bug_reporter.lower() not in self.cc_ignore_list:

            call_data = json.dumps({'cc': {"add": [f'{bug_reporter}']}})
            call_headers = {'Content-Type': 'application/json'}
            # Insert bugzilla server address below
            requests.put('BUGZILLA SERVER ADDRESS /rest.cgi/bug/{0}?Bugzilla_token={1}'
                         .format(bug_number, self.__login_dict['token']), headers=call_headers, data=call_data,
                         verify=False)

    def _get_token(self):
        """
        get new bugzillatoken for bugzilla user to verify to bugzilla.

        get_token(self) -> dict

        :return: dict containing bugzilla token.
        """

        logging.info('Neuer Token wird geholt')

        # Insert bugzilla sever address below
        response = requests.get(f'BUGZILLA SERVER ADDRESS/rest.cgi/login?'
                                f'login={BUGZILLA USER NAME}&password={BUGZILLA PASSWORD}', verify=False)

        return json.loads(response.text)['token']

    def post_email_to_old_bug(self):
        """post email to a bug via bugzilla 5.0.4 web api.

        post_email_to_old_bug(self) -> None

        Function

        post the subject and text of the email to the bug, specified in subject, as a new comment using
        bugzilla 5.0.4 web api.
        """

        for row in self.all_email_dataframe[self.all_email_dataframe['bug_number'] != 0].itertuples():

            if index := (re.search('Von: ["]?BUGZILLA EMAIL ADDRESS', row.email_text) or
                         re.search('Von: IT-Ticket', row.email_text)):
                text = row.email_text[0:index.span()[0]]
            else:
                text = row.email_text

            call_data = json.dumps({'comment': {"body": 'From\n\n{0}\n\n{1}\n\n{2}'
                                   .format(row.email_from, row.email_subject, text.lstrip('\n'))}})
            call_headers = {'Content-Type': 'application/json'}
            # Insert bugzilla server address below
            requests.put('BUGZILLA SERVER ADDRESS/rest.cgi/bug/{0}?Bugzilla_token={1}'
                         .format(row.bug_number, self.__login_dict['token']), headers=call_headers, data=call_data,
                         verify=False)

    def _check_subject_for_bug_number(self, subject):
        """check if there is a bugnumber in the subject of the email and returns it.

        check_subject_for_bug_number(subject -> str) -> int(bugnumber or 0)

        Arguments

        subject     subject of the email you want to check for bugnumber

        Function

        try to find all entries in self.possilbe_bug_numbers in subject and returns the bugnumber.
        If nothing was found 0 will be returned.
        """

        for possible_bug_number in self.possible_bug_numbers:

            if bug_number := re.findall(possible_bug_number, subject):
                return int(bug_number[0])

        return 0

    def _aggregate_bug_numbers(self):
        """aggregate bug numbers for each email if there is [Bug xxx] in email_subject.

        aggregate_bug_numbers(self) -> None

        Function

        aggregate bugnumber for each email if there is [Bug xxx] in the subject of the mail by calling
        check_subject_for_bug_numbers() and put the result to the new column bug_number in all_email_dataframe.
        """

        self.all_email_dataframe['bug_number'] = [self._check_subject_for_bug_number(subject)
                                                  for subject in self.all_email_dataframe['email_subject']]

    def _aggregate_milestones(self):
        """check if there is a milestone in the subject of the email or email_from is corresponding to a milestone
         and returns the milestone found. If no milestone was found, 'default' is returned.

        aggregate_milestone(self) -> None

        Function

        send email_subject and email_from from self.all_email_dataframe line by line to
        self.search_milestone_in_subject().
        """

        self.all_email_dataframe['milestone'] = self.all_email_dataframe[['email_subject', 'email_from']]\
            .apply(lambda x: self._search_milestone_in_subject(x), axis=1)

    def _search_milestone_in_subject(self, search_patterns):
        """check if there is a bugnumber in the subject of the email and returns it.

        search_milestone_in_subject(self, search_patterns -> pandas.tuple) -> String(milestone)

        Arguments

        search_patterns     pandas.tuple including email_subject and of the
        email_from.

        Function

        Function tries to find one of all keys from self.milestone_dict in
        subject and returns the corresponding
        milestone.
        If nothing was found it will return 'IIS'.
        """

        for pattern in self.milestone_dict.keys():

            if milestone_key_found := re.findall(pattern, search_patterns[0][:8].lower()):

                return self.milestone_dict[f'({milestone_key_found[0]})']

        return 'IIS'

    def _cleanup_all_supjects(self):
        """Send all subjects from self.all_email_dataframte to cleanup_subject().

        cleanup_all_subjects(self) -> None

        Function

        send email_subject from self.all_email_dataframe line by line to
        self.cleanup_subject().
        """

        self.all_email_dataframe['email_subject'] = self.all_email_dataframe
        ['email_subject'].apply(
            lambda subject: self._cleanup_subject(subject))

    def _cleanup_subject(self, subject):
        """Removes all patterns from self.possible_bug_numbers,
        self.milestone_dict and self.wg_patterns_list from subject and returns
        it to self.all_emai_dataframe.

        cleanup_sebject(self, subject -> Sting) -> String(cleaned subject)

        Arguments

        subject     String representing the subject of an email.

        Function

        First removes all entries in self.milestone_dict.keys() and
        self.wg_patterns_list.
        Second removes all entries in self.possible_bug_numbers.
        Return cleaned subject.
        """

        for pattern in [patterns for patterns in self.milestone_dict.keys()] + self.wg_patterns_list:
            subject = re.sub(f'{re.sub(r"[)(]+", "", pattern)}', '', subject, flags=re.IGNORECASE)

        for pattern in self.possible_bug_numbers:
            subject = re.sub(f'{pattern}', '', subject, count=1)

        return subject.lstrip()

    def _aggregate_domain_names(self):
        """
        Aggregates the domain names out of email_from for each email by calling aggregate_domain_name(email_from).

        aggregate_domain_names() -> None

        :return: None
        """

        self.all_email_dataframe['domain'] = self.all_email_dataframe['email_from']\
            .apply(lambda email_from: self._aggregate_domain_name(email_from))

    @staticmethod
    def _aggregate_domain_name(email_from):
        """
        Aggregates the domain from email address.

        aggregate_domain_name(email_from ->) -> str

        :param email_from:  Mailaddress the email was send from.

        :return: (Str) Emaildomain
        """

        email_domain = email_from[email_from.find('@'):]

        return email_domain

    @staticmethod
    def _get_list_from_cfg(cfg_path):
        """Returns a list containing all lines from specified cfg_path not starting with #.

        _get_list_from_cfg(cfg_path -> str) -> List

        Open cfg_path and returns all lines not starting with #.
        """

        with open(cfg_path, 'r') as file:
            return [line.rstrip('\n') for line in file.readlines() if not re.match('#', line) and
                    line != '\n']

    def _cleanup_all_email_dataframe(self):
        """
        Removes all emails, witch are not in the allowed_lists.

        cleanup_all_email_dataframe() -> None

        :return: None
        """

        self.all_email_dataframe = self.all_email_dataframe[
            self.all_email_dataframe['domain'].isin(self.allowed_domains_list) |
            self.all_email_dataframe['email_from'].isin(self.allowed_emails_address_list)]

    def main(self):
        """Main Module to run the class as a script

        main() -> none

        Running the class as a script.
        """

        self.get_emails()
        self._aggregate_bug_numbers()
        self._aggregate_milestones()
        self._cleanup_all_supjects()
        self._aggregate_domain_names()
        self._cleanup_all_email_dataframe()
        self.post_email_to_old_bug()
        self.create_new_bug()
        self.post_attachments_to_bug()


if __name__ == '__main__':

    warnings.simplefilter('ignore', InsecureRequestWarning)
    pd.option_context('display.max_rows', None, 'display.max_columns', None)

    logging.info('Script startet')

    inst = EmailToBug()
    inst.main()

    logging.info('Script beendet')

    exit()
