# emails to bugs

email_to_bugs is a python 3.8 script to create an comment bugs in bugzilla 5.0.4
via emails, using IMAP and bugzilla REST API.

## Installation

Clone repository

git clone https://gitlab.com/uath4u/email_to_bugzilla_5-0-4_bug.git

Install dependencies with pip.

pip3 install requirements.txt

## Configuration

Crate a folder called 'erstellte_bugs' in your IMAP /INBOX folder.

Set all domains you want to allow emails to go to bugzilla in
'allowed_domains_list.txt'.

Set all email addresses you want to allow to got to bugzilla in
'allowed_email_addresses.txt'. You don't need to do this, if the domain is
already in the allowed_domains.txt.

Put your imap, smtp und bugzilla credentials and server names to the script.

Search for

- IMAP USER NAME
- IMAP PASSWORD
- IMAP SERVER NAME

and replace it with your credentials or server address.

Search for:

- SMTP SERVER NAME
- SMTP PASSWORD
- SMTP SERVER NAME

and replace it with your credentials or server address.

Search for:

- BUGZILLA SERVER ADRESS
- BUGZILLA USER NAME
- BUGZILLA PASSWORD

and replace it with your credentials or server address.

Search for RESPONSE TEXT and insert your email text for your response mail.

## Usage

You cann now write to your email account and all emails, found by the script
will trtigger one of the following actions.

Create a new bug:

If you send an email to the email acccount without a bugnumber in the subject, a
new bug will be created and a confirmation email will be send back to the email
sender address.

Update a bug.

If you send an email to the email account containing a bugnumber at the start of
the subject in one of the following formats ('#xxxx:', '[Ticket xxxx]' or
'[Bug xxx]' with x = 1-9) the email will be added to the mentioned bug as a
comment.

All email attachments will be attached to the created or updated bug.

If there ara autoresponder mails incoming, the script sort them out.
